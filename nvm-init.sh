#!/bin/bash

source /etc/profile.d/nvmsetup.sh
nvm install --lts
nvm use default

# yes i know yarn says not to install it this way
npm install -g npm
npm install -g yarn

#YARN="/usr/local/nvm/versions/node/v*/bin/yarn"

export PATH="$PATH:/usr/local/nvm/versions/node/v*/bin"

NPM_PKGS="
    create-react-app
    jetpack
    now
    prettier
"

for PKG in $NPM_PKGS ; do
    yarn global add $PKG
done
