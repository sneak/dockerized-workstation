#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

# setup mirror source
MRUM="main restricted universe multiverse"
cat > /etc/apt/sources.list << EOF
deb mirror://mirrors.ubuntu.com/mirrors.txt bionic $MRUM
deb mirror://mirrors.ubuntu.com/mirrors.txt bionic-updates $MRUM
deb mirror://mirrors.ubuntu.com/mirrors.txt bionic-security $MRUM
deb mirror://mirrors.ubuntu.com/mirrors.txt bionic-backports $MRUM
EOF

# update
apt update && apt upgrade -y

# deliberate decision to use distro docker and not latest
PKGS="
    apt-file
    apt-utils
    bash
    bash-completion
    build-essential
    byobu
    command-not-found
    cryptsetup-bin
    docker.io
    gconf-service
    gconf2
    git
    gnupg-agent
    gnupg2
    golang-go
    gvfs-bin
    haveged
    htop
    jq
    libasound2
    libcurl4
    libgtk-3-0
    libnotify4
    libnss3
    libx11-xcb1
    libxkbfile1
    libxss1
    libxtst6
    lsof
    mosh
    nmap
    ntp
    ntpdate
    opensc
    openssh-server
    pbzip2
    pcscd
    pinentry-curses
    pv
    python-pip
    python3-dev
    python3-pip
    rsync
    ruby
    scdaemon
    socat
    software-properties-common
    strace
    vim
    xdg-utils
"

apt install -y $PKGS

# update apt-file cache
apt-file update
