REMOTEHOST := root@las2.bounce
IMAGENAME := sneak/dockerized-workstation
YYYYMMDD := $(shell date +%Y%m%d)

default: remote-build

build:
	docker build -t $(IMAGENAME):$(YYYYMMDD) .

upload:
	rsync -avP --delete ./ $(REMOTEHOST):dockerized-workstation/

remote-build: upload
	ssh -t $(REMOTEHOST) "cd dockerized-workstation && make build"

publish:
	ssh -t $(REMOTEHOST) "cd dockerized-workstation && docker push $(IMAGENAME):$(YYYYMMDD)"

tryout:
	ssh -t $(REMOTEHOST) "docker run -ti $(IMAGENAME)"
