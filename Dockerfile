# v0.11 -> ubuntu 18.04 bionic
FROM phusion/baseimage:0.11

ARG AUTHORIZED_KEYS_URL=https://sneak.cloud/authorized_keys

ADD ./01ubuntu.sh /tmp/01ubuntu.sh
RUN bash /tmp/01ubuntu.sh && rm /tmp/01ubuntu.sh

RUN useradd -m user
ADD $AUTHORIZED_KEYS_URL /root/.ssh/authorized_keys
ADD $AUTHORIZED_KEYS_URL /home/user/.ssh/authorized_keys

########################################################
#####  python3 stuff
########################################################
RUN pip3 install --upgrade pip
RUN pip3 install awscli
RUN pip3 install httpie
RUN pip3 install pipenv
RUN pip3 install powerline-shell

########################################################
#####  ruby
########################################################
RUN gem install bundler

########################################################
#####  atom editor
########################################################
ADD https://atom.io/download/deb /tmp/atom.deb
RUN dpkg -i /tmp/atom.deb && rm /tmp/atom.deb

########################################################
#####  nvm and node and yarn and js packages
########################################################
RUN git clone https://github.com/creationix/nvm.git /usr/local/nvm
ADD ./nvmsetup.sh /etc/profile.d/nvmsetup.sh
ADD ./nvm-init.sh /tmp/nvm-init.sh
RUN bash /tmp/nvm-init.sh && rm /tmp/nvm-init.sh
